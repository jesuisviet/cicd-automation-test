My first automation test in NodeJS using Gitlab CI

[![pipeline status](https://gitlab.com/jesuisviet/cicd-automation-test/badges/master/pipeline.svg)](https://gitlab.com/jesuisviet/cicd-automation-test/-/commits/master)
[![coverage report](https://gitlab.com/jesuisviet/cicd-automation-test/badges/master/coverage.svg)](https://gitlab.com/jesuisviet/cicd-automation-test/-/commits/master)

[![Latest Release](https://gitlab.com/jesuisviet/cicd-automation-test/-/badges/release.svg)](https://gitlab.com/jesuisviet/cicd-automation-test/-/releases)
